<?php

require_once('../Model/Citas.php');

$paciente = $_POST['nombre_paciente'];
$medico = $_POST['nombre_medico'];

$citas = new Citas();
$result = $citas->registrarCitas($paciente, $medico);

if($result) {
    /**mensaje de operación satisfactoria y redirección al home page */
    echo "<script>alert('Cita registrada correctamente')</script>";
	echo "<script>window.location.replace('../View/home.php');</script>"; 

}