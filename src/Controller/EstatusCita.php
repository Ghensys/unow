<?php

require_once('../Model/Citas.php');

$cita = $_POST['id_citas'];
$estatus = $_POST['nuevo_estatus'];

$citas = new Citas();
$result = $citas->actualizarEstatusCitas($cita, $estatus);

if($result) {
    /**Mensaje de operación satisfactoria y redirección al listado de citas */
    echo "<script>alert('Estatus actualizado correctamente.')</script>";
	echo "<script>window.location.replace('".$_SERVER["HTTP_REFERER"]."');</script>"; 
}
else {
    /**mensaje de operación fallida y redirección al listado de citas */
    echo "<script>alert('Error, vuelva a intentarlo.')</script>";
	echo "<script>window.location.replace('".$_SERVER["HTTP_REFERER"]."');</script>"; 
}