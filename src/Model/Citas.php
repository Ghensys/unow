<?php

include '../../conf/connection.php';

Class Citas extends DbConnect{

    private $conn;

    function __construct() {
        /**Conexion a la bd */
        $dbcon = new parent();
        $this->conn = $dbcon->connect();
        $date = new DateTime();
        $this->fecha = $date->format('Y-m-d H:i:s');
    }

    /** Metodo de registro */
    public function registrarCitas($nombre_paciente, $nombre_doctor) {

        $sql = 'INSERT INTO citas_medicas.citas (nombre_paciente, nombre_doctor, fecha, estatus) VALUES ("'.$nombre_paciente.'", "'.$nombre_doctor.'", "'.$this->fecha.'", 0);';
        $data = mysqli_query($this->conn, $sql) or die (mysqli_errno($this->conn));

        if($data) {
            return $data;
        }
        else {
            return false;
        }
    }

    /** Metodo de consulta */
    public function consultarCitas($medico) {

        $caracteres = array('%', '+');
        $sql = str_replace($caracteres, ' ', 'SELECT * FROM citas_medicas.citas WHERE nombre_doctor = "'.$medico.'";');
        $data = mysqli_query($this->conn, $sql) or die (mysqli_errno($this->conn));
        return $data;
    }

    /** Metodo de actualización para aprobación o rechazo de las citas*/
    public function actualizarEstatusCitas($cita, $estatus) {

        $sql = 'UPDATE citas_medicas.citas SET estatus = '.$estatus.' WHERE (id_citas = '.$cita.');';
        $data = mysqli_query($this->conn, $sql) or die (mysqli_errno($this->conn));
        return $data;
    }
}