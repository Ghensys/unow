<?php

include 'header.php';

?>
    <div class="container">
        <h1>Listado de citas</h1>

        <?php
            while($data = mysqli_fetch_assoc($consulta) ) {
        ?>

            <div class="card">
                <div class="card-header">
                    Paciente: <?php echo $data['nombre_paciente'] ?> 
                </div>
                <div class="card-body">
                    <h5 class="card-title"># <?php echo $data['id_citas'] ?> | Fecha: <?php echo $data['fecha']; ?></h5>
                    <p class="card-text">Descripción de sintomas.</p>
                    <p class="card-text">Estatus:
                        <?php
                            if($data['estatus'] == 0) {
                                echo "Pendiente.</p>";
                        ?>
                    <form action="../Controller/EstatusCita.php" method="post">
                        <input type="hidden" name="id_citas" value="<?php echo $data['id_citas'] ?>"/>
                        <input type="hidden" name="nuevo_estatus" value="1"/>
                        <button id="registrar" type="submit" class="btn btn-success">Aceptar</button>
                    </form>
                    <form action="../Controller/EstatusCita.php" method="post">
                        <input type="hidden" name="id_citas" value="<?php echo $data['id_citas'] ?>"/>
                        <input type="hidden" name="nuevo_estatus" value="2"/>
                        <button id="registrar" type="submit" class="btn btn-danger">Rechazar</button>
                    </form>
                        <?php
                            } 
                            if($data['estatus'] == 1) {
                                echo "Aceptada.</p>";
                        ?>
                    <form action="../Controller/EstatusCita.php" method="post">
                        <input type="hidden" name="id_citas" value="<?php echo $data['id_citas'] ?>"/>
                        <input type="hidden" name="nuevo_estatus" value="1"/>
                        <button id="registrar" type="submit" class="btn btn-success disabled" disabled>Aceptar</button>
                    </form>
                    <form action="../Controller/EstatusCita.php" method="post">
                        <input type="hidden" name="id_citas" value="<?php echo $data['id_citas'] ?>"/>
                        <input type="hidden" name="nuevo_estatus" value="2"/>
                        <button id="registrar" type="submit" class="btn btn-danger">Rechazar</button>
                    </form>
                        <?php
                            }
                            if($data['estatus'] == 2) {
                                echo "Rechazada.</p>";
                            }
                        ?>
                </div>
            </div>
            <br>

        <?php
        }
        ?>

    </div>

<?php

include 'footer.php';