<?php

include 'header.php';

?>
    <div class="container">
        <h1>Pagina Principal - Aplicación de Consultas Medicas</h1>

        <br>

        <form method="get" action="../Controller/ConsultarCitas.php">
            <label for="nombre_medico" class="form-label">Consulta de citas</label>
            <select class="form-select" name="medico" id="nombre_medico" required>
                <option value="" selected>Seleccionar medico</option>
                <option value="Medico 1">Medico 1</option>
                <option value="Medico 2">Medico 2</option>
                <option value="Medico 3">Medico 3</option>
            </select>
            <button id="buscar" type="submit" class="btn btn-primary">Buscar</button>
        </form>
    </div>

<?php

include 'footer.php';