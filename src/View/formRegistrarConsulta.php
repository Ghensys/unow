<?php

include 'header.php';

?>

	<div class="container">
		<h1>Formulario de solicitud</h1>

		<!-- Formulario de registro de pacientes -->
		<form method="POST" action="../Controller/RegistrarCitas.php">
			<div class="mb-3">
				<label for="nombre_paciente" class="form-label">Nombre del Paciente</label>
				<!-- Nombre del paciente -->
				<input type="text" class="form-control" name="nombre_paciente" id="nombre_paciente" required/>
			</div>
			<div class="mb-3">
				<label for="nombre_medico" class="form-label">Nombre del Medico</label>
				<!-- Selección del medico -->
				<select class="form-select" aria-label="Default select example" name="nombre_medico" required>
				<option value="" selected disabled>Seleccionar</option>
				<option value="Medico 1">Medico 1</option>
				<option value="Medico 2">Medico 2</option>
				<option value="Medico 3">Medico 3</option>
				</select>
			</div>
			<button id="registrar" type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>

<?php

include 'footer.php';